      program circle
      real r, area

c     This program reads a real number r and prints
c     the area of a circle with radius r.
c     (fortran 77 code) (http://web.stanford.edu/class/me200c/tutorial_77/)
      
      write (*,*) 'Give radius r:'
      read (*,*) r
      area = 3.14159 *r *r
      write (*,*) 'Area = ', area

      stop
      end

      program arithmetic_if
      integer i

*     demonstration of arithmetic if

      write (*,*) 'Give an integer around 0:'
      read (*,*) i
      if (i) 10,20,30
      stop
 
 10   write (*,*) 'Less than 0'
      stop
 20   write (*,*) 'Equals to 0'
      stop
 30   write (*,*) 'Greater than 0'
      stop

      end

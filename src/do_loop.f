      program do_loop
      integer i,j,k,sum

c     demonstration of do_loop

      write (*,*) 'Give me ''from'' index i'
      read (*,*) i
      write (*,*) 'Give me ''to'' index j (.gt. i)'
      read (*,*) j

      sum = 0
      do 10 k = i, j
c     you cannot change index variable now, this is error:
c     k = k +1
         sum = sum +1
         write (*,*) 'k = ', k
         write (*,*) 'sum = ', sum
 10   continue

      write (*,*)
     1     '...but notice we can change upper boundary value',
     2     ' inside that loop,'

      do 20 k = i, j
         j = j +1
         write (*,*) 'k = ', k, '\t\tj = ', j
 20   continue

      write (*,*)
     &     'start, stop and step values evaluated only',
     &     ' once at start.'

      stop
      end

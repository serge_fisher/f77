
c     --------------------------------------
c     program to multiply matrix by a vector

      program matrix_by_vector_mult
      integer i,j
      parameter (m=4, n=10)
      real Matrix(m,n), vector(n), result(m)
      common /matrix/Matrix,vector
      
      data Matrix/
     +     10.0, 20.0, 30.0, 40.0,
     1     11.0, 21.0, 31.0, 41.0,
     2     12.0, 22.0, 32.0, 42.0,
     3     13.0, 23.0, 33.0, 43.0,
     4     14.0, 24.0, 34.0, 44.0,
     5     15.0, 25.0, 35.0, 45.0,
     6     16.0, 26.0, 36.0, 46.0,
     7     17.0, 27.0, 37.0, 47.0,
     8     18.0, 28.0, 38.0, 48.0,
     9     19.0, 29.0, 39.0, 49.0 /

      data vector/9.0, 5.0, 3.0, 1.0, 8.0, 3.0, 2.0, 1.0, 0.0, 89.1/

c     this is defined in separate module
      call matvec(m,n,Matrix,m,vector,result)

      write (*,*) 'Matrix:'
      do 70 i = 1,m
         write (*,111) '[',i,']', (Matrix(i,j), j=1,n)
 70   continue
 111  format (A,I1,A, 10(F6.2))

      write (*,*) 'Vector:'
      write (*,111) '[',1,']', (vector(j), j=1,n)

      write (*,*) 'Result:'
      write (*,*) result(1),result(2),result(3),result(4)
      
      stop
      end


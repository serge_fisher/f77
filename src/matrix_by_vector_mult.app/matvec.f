c     Compute y = A*x, where A is m by n and stored in an array with
c     leading dimenstion lda
      
      subroutine matvec (m,n,A,ldofA, x,y)
      integer m,n, ldofA
c     note we can use * only for the last dimension
      real x(*),y(*), A(ldofA, *)

      integer i,j

c     initialize y
      do 20 i = 1,m
         y(i) = 0.0
 20   continue

c     Matrix-vector product by saxpy on columns in A.
c     Notice that the length of each column of A is m, not n!
c     Notice we use A(1,j) as a vector (vector address or pointer)
      do 30 j = 1,n
         call saxpy (m,x(j), A(1,j), y)
 30   continue

      return
      end

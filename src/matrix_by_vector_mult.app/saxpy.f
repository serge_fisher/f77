c     saxpy: compute y := alpha*x + y,
c     where x and y are vectors of length n (at least)

      subroutine saxpy (n, alpha, x, y)
      integer n
      real alpha, x(*), y(*)

      integer i

      do 10 i = 1, n
         y(i) = alpha * x(i) + y(i)
 10   continue

      return
      end

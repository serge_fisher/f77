      program variable_len_arrays

      real vec1(10), vec2(10)
      integer i

      do 10 i = 1, 10
         vec1(i) = i * 1.0
         vec2(i) = 1.0
 10   continue

      do 30 i = 1, 10
         write (*,*) 'vector1 = ', vec1(i), ' vector2 = ', vec2(i)
 30   continue

      write (*,*) '-----------------'
      call saxpy(10, 1.2, vec1, vec2)

      do 20 i = 1, 10
         write (*,*) 'vector1 = ', vec1(i), ' vector2 = ', vec2(i)
 20   continue

      stop
      end
      
      subroutine saxpy (n, alpha, x, y)
      integer n
      real alpha, x(*), y(*)

c     x(*) means array with unknown size

      integer i

      do 10 i = 1, n
         y(i) = alpha * x(i) + y(i)
 10   continue

      return
      end

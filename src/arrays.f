      program arrays

c     arrays demo

c     this will create a vector with index 1..20
      real a(20)

c     this will create a vector with index 0..19 (same size)
      real b(0:19)

c     this will create a vector with indexes from -123 to +123
      logical aaa(-123:123)

c     alternate initializer using 'dimension' keyword
      real samea, sameb
      dimension samea(20)
      dimension sameb(0:19)

c     access to i-th element is by a(i)
      integer i

      do 100 i = 1, 20
         a(i) = i ** 2
 100  continue

      do 200 i = 1, 20
         write (*,*) 'i=', i, 'a(i) = ', a(i)
 200  continue

      stop
      end

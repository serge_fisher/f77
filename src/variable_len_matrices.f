      program variable_len_matirices
      parameter (m=4, n=5)
      real Matrix(m,n), vector(n), result(m)
      integer i,j

      data Matrix/ 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0,
     +            11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0,19.0,20.0/

      data vector/5.0, 3.0, 7.0, 2.0, 9.0/

c     now we multiply Matrix by vector
      call matvec(m,n,Matrix,m,vector,result)

      write (*,*) 'Matrix:'
      do 70 i = 1,m
         write (*,*) Matrix(i,1), Matrix(i,2), Matrix(i,3),
     +               Matrix(i,4), Matrix(i,5)
 70   continue

      write (*,*) 'Vector:'
      write (*,*) vector(1),vector(2),vector(3),vector(4), vector(5)

      write (*,*) 'Result:'
      write (*,*) result(1),result(2),result(3),result(4)
      
      stop
      end

c     =================================

c     Compute y = A*x, where A is m by n and stored in an array with
c     leading dimenstion lda
      
      subroutine matvec (m,n,A,ldofA, x,y)
      integer m,n, ldofA
c     note we can use * only for the last dimension
      real x(*),y(*), A(ldofA, *)

      integer i,j

c     initialize y
      do 20 i = 1,m
         y(i) = 0.0
 20   continue

c     Matrix-vector product by saxpy on columns in A.
c     Notice that the length of each column of A is m, not n!
c     Notice we use A(1,j) as a vector (vector address or pointer)
      do 30 j = 1,n
         call saxpy (m,x(j), A(1,j), y)
 30   continue

      return
      end

c     ---------------------------------

c     saxpy: compute y := alpha*x + y,
c     where x and y are vectors of length n (at least)

      subroutine saxpy (n, alpha, x, y)
      integer n
      real alpha, x(*), y(*)

      integer i

      do 10 i = 1, n
         y(i) = alpha * x(i) + y(i)
 10   continue

      return
      end

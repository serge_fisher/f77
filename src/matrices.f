      program matrices

c     matrices (two-dimensional arrays) demo

c     3x5 matrix with reals (3 rows, 5 columns; overall 15 elements)
      real A(3,5)

c     it will look like this in memory:
c     (1,1) (1,2) (1,3) (1,4) (1,5)
c     (2,1) (2,2) (2,3) (2,4) (2,5)
c     (3,1) (3,2) (3,3) (3,4) (3,5)

c     element (1,2) will immidiately follow element (3,1) in memory.
      
c     elements of an array is not initialized by runtime, it needs to be
c     set up by hand to zero or whatever.

c     there is no mechanizm for dynamic storage allocation, so declare
c     as much as it might need, and use what is needed to use right now.

      integer i,j

c     the address of element (i,j) is given by
c     addr[ A(i,j) ] = addr[ A(1,1) ] + (j-1)*lda + (i-1)
c     where lda is leading dimension (row) size
c     i.e. in our A lda is 3

      do 20 j = 1, 4
         do 10 i = 1, 3
            a(i,j) = real(i) / real(j)
 10      continue
 20   continue

c     the last column is not used. this is perfectly normal, but beware
c     reserving rows instead of columns - this creates spaces in memory

c     fortran 77 allows arrays of up to seven dimensions. The syntax and
c     storage format are analogous to the two-dimensional case, so we
c     will not spend time on this.

      do 40 j = 1, 4
         do 30 i = 1, 3
            write (*,*) 'i = ', i, 'j = ', j, 'a(i,j) = ', a(i,j)
 30      continue
 40   continue

      stop
      end

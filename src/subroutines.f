      program subroutines
      integer m,n
      
c     demo of subroutine usage

c     subroutine name (list-of-arguments)
c     declarations
c     statements
c     return
c     end

      m = 1
      n = 2

c     note the call-by-reference paradigm
      call iswap(m,n)

      write(*,*) m, n

      stop
      end

c     usually subroutines should be declared in different file outside
c     the caller program module. For sake of demo only I left the
c     subroutine declaration here in the same file.
      
      subroutine iswap (a,b)
      integer a,b
c     local variable
      integer tmp

      tmp = a
      a = b
      b = tmp

      return
      end
      

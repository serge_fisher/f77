      program rain
c     user-defined function demo

c     here 'r' is not a variable but function
      real r, t, sum

      integer m

      write (*,*) 'give me t, please'
      read (*,*) t
      sum = 0.0
      do 10 m = 1, 12
         sum = sum + r(m,t)
 10   continue

      write (*,*) 'Annual rainfall is ', sum, 'inches'

      stop
      end
      
c     -------------------
c     function has a type
      real function r(m,t)
c     here we define the type of arguments
      integer m
      real t

c     retval should be named as a function itself
      r = 0.1*t * (m**2 + 14*m +46)
      if (r .LT. 0) r = 0.0

c     return statement instead of stop
      return
      end

c     some predefined (built-in) functions are
c     abs     absolute value
c     min     minimum value
c     max     maximum value
c     sqrt    square root
c     sin     sine
c     cos     cosine
c     tan     tangent
c     atan    arctangent
c     exp     exponential (natural)
c     log     logarithm (natural)

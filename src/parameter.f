      program parameter
      real r, area
      parameter (pi = 3.1415926535897, e=2.71)

c     demonstration of constant usage (parameter keyword)

      write (*,*) 'give me a radius: '
      read (*,*) r

      area = r *pi *r

      write (*,*) 'Area = ', area

      stop
      end

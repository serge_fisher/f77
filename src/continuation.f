      program continuation
      real area,r

c     this demonstrates line continuation.

      write (*,*)
     &     'Give radius r:'
      read (*,*) r

      area = 3.14159265358979
     +     * r * r

c     continuation characters can be any, but it is considered good
c     programming style to use either '+', '&', or digits as number
c     for countinued line

      write
     1     (*,*)
     2     'Area = ',
     3     area

      stop
      end

# supress lazy evaluation (needed to use it as a dependency for 'all')
OUTFILES != echo `for i in src/*.f; do echo bin/$$(basename $$i .f); done`
APPS != echo `for i in src/*.app; do echo app/$$(basename $$i .app); done`

# well actually in OpenBSD there is no 'f77' symlink by default
COMPILER := gfortran

OPTS := -time -x f77

.PHONY: clean
.SUFFIXES: .f

all: bin $(OUTFILES) app $(APPS)

bin:
	mkdir -p bin

app:
	mkdir -p app

# bsd make complains about $< being GNUism here
$(OUTFILES):
	$(COMPILER) -o $@ $(OPTS) src/`basename $@`.f

$(APPS):
	$(COMPILER) -o $@ $(OPTS) src/`basename $@`.app/*.f

clean:
	rm -f *.o a.out
	rm -f $(OUTFILES)
	rm -f $(APPS)

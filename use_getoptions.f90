! This software is distributed under the following terms:
! 
! Copyright (C) 2005 Dominik Epple
! 
! Redistribution and use in source and binary forms, with or without
! modification, are permitted provided that the following conditions
! are met:
! 1. Redistributions of source code must retain the above copyright
!    notice, this list of conditions and the following disclaimer.
! 2. Redistributions in binary form must reproduce the above copyright
!    notice, this list of conditions and the following disclaimer in the
!    documentation and/or other materials provided with the distribution.
!
! THIS SOFTWARE IS PROVIDED BY AUTHOR AND CONTRIBUTORS ``AS IS'' AND
! ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
! ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
! FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
! DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
! OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
! HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
! LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
! OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
! SUCH DAMAGE.

      program use_getoptions
        use getoptions
        implicit none

        character :: okey

        character(len=80) :: filename='def.in', ofilename='def.out'
        integer :: i=1
        integer :: binary=0
        double precision :: d=0
        real :: r=0

        write(6,*)
        write(6,*) 'Parsing arguments and parameters:'
        write(6,*) '---------------------------------'
        write(6,*)

        do
          okey=getopt('i:br:d:o:')
          if(okey.eq.'>') exit
          if(okey.eq.'!') then
            write(6,*) 'unknown option: ', trim(optarg)
            stop
          end if
          if(okey.eq.'b') then
            binary=1
            write(6,*) 'Found argument: -',okey
          end if
          if(okey.eq.'i') then
            read(optarg,'(i)') i
            write(6,*) 'Found argument: -',okey,' with value ',trim(optarg)
          end if
          if(okey.eq.'d') then
            read(optarg,'(f)') d
            write(6,*) 'Found argument: -',okey,' with value ',trim(optarg)
          end if
          if(okey.eq.'o') then
            ofilename=optarg
            write(6,*) 'Found argument: -',okey,' with value ',trim(optarg)
          end if
          if(okey.eq.'r') then
            read(optarg,'(f)') r
            write(6,*) 'Found argument: -',okey,' with value ',trim(optarg)
          end if
          if(okey.eq.'.') then
            filename=optarg
            write(6,*) 'Found parameter: ',trim(filename)
          end if
        end do

        write(6,*)
        write(6,*) 'Got these arguments and parameters:'
        write(6,*) '-----------------------------------'
        write(6,*)

        write(6,*) 'binary:',binary
        write(6,*) 'integer:',i
        write(6,*) 'double:',d
        write(6,*) 'real:', r
        write(6,*) 'filename:',trim(filename)
        write(6,*) 'ofilename:',trim(ofilename)
      end program
